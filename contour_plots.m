function [C_f, C_ripple_i_L] = contour_plots(source_directory, target_directory)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

frequency_data = importdata([source_directory, '/frequency.dat'],';');
v = 3e3;
C_f = save_contour(frequency_data, v, 'contour_frequency.dat');

ripple_i_L_data = importdata([source_directory, '/ripple_i_L.dat'],';');
v = 2;
C_ripple_i_L = save_contour(ripple_i_L_data, v, 'contour_ripple_i_L.dat');

    function [contour] = save_contour(contour_data, value, filename)
        k_i = contour_data.data(:,1);
        k_v = contour_data.data(:,2);
        z = contour_data.data(:,3);
        
        base = 1e3;
        step = 1e3;
        upper_bound = 200e3;
        Xs = base : step : upper_bound;
        [k_i_q, k_v_q] = meshgrid(Xs, Xs);
        
        z_q = griddata(k_i, k_v, z, k_i_q, k_v_q);
        
        C = contourc(Xs, Xs, z_q, [value,value]);
        
        contour = Contour(C);
        if ~ isempty(target_directory)
            contour.save_contour(target_directory, filename)
        end
    end

end

