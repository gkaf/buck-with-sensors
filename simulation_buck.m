% z-source/simulation.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

import_flow;

[dynamicsFunctionBuck, switchingFunctionBuck, epsilon] = getBuck();
circuit = CircuitBuck(dynamicsFunctionBuck, switchingFunctionBuck);

Dt_fin = 0.001;
Dt_burn_in = 0.01;

e_tol = 1e-8;
rel_tol = e_tol;
abs_tol = e_tol .* ones(size(circuit.nominal_state()));
max_step = 1e-5;
state_gen = FlowDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
    circuit, ...
    epsilon, ...
    Dt_fin, Dt_burn_in);

ha = HybridAutomaton(state_gen);

Dt_state_ub = 0.2;
Dt_ub = 1.6;
% Selecting initial state on the sliding mode:
t_0 = 0;
x_0 = circuit.nominal_state();
% x_0 = circuit.dynamicsFunctionsSurf.physical_space([0;0;40]);

discreteStateData = Transcript(state_gen);
u_0 = 1;

initial_stateId = StateId(FlowDiscreteStateEnumeration.Initial, discreteStateData, u_0);
hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);

transcript = hae.evaluate();

[ts, ys, us] = transcript.get_tyu();

path(pathdef);