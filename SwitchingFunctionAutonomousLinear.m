classdef SwitchingFunctionAutonomousLinear < SwitchingFunction
    %UNTITLED6 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        J_H
        zx_ref
        
        T_trs
        T_inv
    end
    
    methods
        function [sf] = SwitchingFunctionAutonomousLinear(J_H, zx_ref)
            %UNTITLED6 Construct an instance of this class
            %   Detailed explanation goes here
            sf@SwitchingFunction();
            
            sf.J_H = J_H;
            sf.zx_ref = zx_ref;
            
            null_J_H = null(sf.J_H);
            for idx = 1:size(null_J_H, 2)
                nrm = norm(null_J_H(:,idx), 2);
                null_J_H(idx,:) = null_J_H(idx,:).*(1/(nrm^2));
            end
            
            sf.T_trs = [sf.J_H; ctranspose(null_J_H)];
            sf.T_inv = inv(sf.T_trs);
        end
        
        function [h] = switching_function(sf, ~, zx)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            h = sf.J_H*(zx - sf.zx_ref);
        end
        
        function [zz] = normal_space(fds, zx)
            zz = fds.T_trs*(zx - fds.zx_ref);
        end
        
        function [zx] = physical_space(fds, zz)
            zx = fds.T_inv*zz + fds.zx_ref;
        end
        
        function [zx] = nominal_state(fds)
            zx = fds.zx_ref;
        end
        
        function [u] = vector_field(~, w)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            u = w;
        end
        
        function [v] = potential_function(~, ~, h)
            %potential_function Summary of this method goes here
            %   Detailed explanation goes here
            v = h.^2;
        end
        
        function [del_zx_V, del_t_V] = potential_gradient(sf, ~, ~, ~)
            del_zx_V = sf.J_H;
            del_t_V = 0;
        end
    end
end

