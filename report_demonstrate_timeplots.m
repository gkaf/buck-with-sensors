function [T_buck, T_buck_with_sensors, T_periods] = report_demonstrate_timeplots(folder)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

e_tol = 1e-8;
max_step = 1e-5;

[dynamicsFunctionBuck, switchingFunctionBuck, epsilon_buck] = getBuck();
[dynamicsFunctionBuckWithSensors, switchingFunctionBuckWithSensors, epsilon_buck_with_sensors] = getBuckWithSensors();

circuit_buck = CircuitBuck(dynamicsFunctionBuck, switchingFunctionBuck);
circuit_buck_with_sensors = CircuitBuckWithSensors(dynamicsFunctionBuckWithSensors, switchingFunctionBuckWithSensors);

[ts_buck, ~, ~] = detect_cycle(circuit_buck, epsilon_buck);
[ts_buck_with_sensors, ~, ~] = detect_cycle(circuit_buck_with_sensors, epsilon_buck_with_sensors);
Tp_buck = ts_buck(end) - ts_buck(1);
Tp_buck_with_sensors = ts_buck_with_sensors(end) - ts_buck_with_sensors(1);

[ts_buck, ys_buck, us_buck] = trajectory(circuit_buck, epsilon_buck);
[ts_buck_with_sensors, ys_buck_with_sensor, us_buck_with_sensor] = trajectory(circuit_buck_with_sensors, epsilon_buck_with_sensors);

T_buck = table(ts_buck', ys_buck(1,:)', ys_buck(2,:)', us_buck', ...
    'VariableNames', {'t', 'y_1', 'y_2', 'u'});

T_buck_with_sensors = table(ts_buck_with_sensors', ys_buck_with_sensor(1,:)', ys_buck_with_sensor(2,:)', ...
    ys_buck_with_sensor(3,:)', ys_buck_with_sensor(4,:)', us_buck_with_sensor', ...
    'VariableNames', {'t', 'y_1', 'y_2', 'y_3', 'y_4', 'u'});

T_periods = table(Tp_buck, Tp_buck_with_sensors, 'VariableNames', {'T_buck', 'T_buck_with_sensors'});

if ~isempty(folder)
    filename = [folder, '/', 'Resources/Plots/demo-time-plot/data/buck.dat'];
    writetable(T_buck, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [folder, '/', 'Resources/Plots/demo-time-plot/data/buck_with_sensors.dat'];
    writetable(T_buck_with_sensors, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [folder, '/', 'Resources/Plots/demo-time-plot/data/periods.dat'];
    writetable(T_periods, filename, 'WriteVariableNames', true, 'Delimiter', ';');
end

    function [ts, ys, us] = trajectory(circuit, epsilon)
        import_flow;
        
        Dt_fin = 0.001;
        Dt_burn_in = 0.01;
        
        rel_tol = e_tol;
        abs_tol = e_tol .* ones(size(circuit.nominal_state()));
        state_gen = FlowDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
            circuit, ...
            epsilon, ...
            Dt_fin, Dt_burn_in);
        
        ha = HybridAutomaton(state_gen);
        
        Dt_state_ub = 0.2;
        Dt_ub = 1.6;
        % Selecting initial state on the sliding mode:
        t_0 = 0;
        x_0 = circuit.nominal_state();
        % x_0 = circuit.dynamicsFunctionsSurf.physical_space([0;0;40]);
        
        discreteStateData = Transcript(state_gen);
        u_0 = 1;
        
        initial_stateId = StateId(FlowDiscreteStateEnumeration.Initial, discreteStateData, u_0);
        hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);
        
        transcript = hae.evaluate();
        
        [ts, ys, us] = transcript.get_tyu();
        
        ts = ts - ts(1).*ones(size(ts));
        
        path(pathdef);
    end

    function [ts, ys, us] = detect_cycle(circuit, epsilon)
        import_cycle_detection;
        
        
        rel_tol = e_tol;
        abs_tol = e_tol .* ones(size(circuit.nominal_state()));
        
        max_depth = 8;
        eta_tolerance = 1e-6;
        Dt_cycle = 1;
        Dt_cross = 0.2;
        
        state_gen = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
            circuit, ...
            epsilon, max_depth, eta_tolerance, ...
            Dt_cross, Dt_cycle);
        
        ha = HybridAutomaton(state_gen);
        
        Dt_state_ub = 0.4;
        Dt_ub = 2;
        % Selecting initial state on the sliding mode:
        t_0 = 0;
        x_0 = circuit.nominal_state();
        % x_0 = circuit.dynamicsFunctionsSurf.physical_space([0;0;40]);
        
        u_0 = 1;
        initial_stateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, {}, u_0);
        hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);
        
        cycleAnalysisResult = hae.evaluate();
        
        [ts, ys, us] = cycleAnalysisResult.get_tyu();
        
        path(pathdef);
    end
end

