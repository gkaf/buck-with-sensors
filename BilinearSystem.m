classdef (Abstract) BilinearSystem < DynamicsFunction
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % Parameters
        A
        B
        a
        b
    end
    
    methods
        function [fds] = BilinearSystem(U, ...
                A, B, a, b)
            %DynamicsFunction Construct an instance of this class
            %   Detailed explanation goes here
            fds@DynamicsFunction(U);
            fds.A = A;
            fds.B = B;
            fds.a = a;
            fds.b = b;
        end
        
        function [dx] = f_der(fds, ~, x, u)
            %f_der Summary of this method goes here
            %   Detailed explanation goes here
            [~,~,p] = size(fds.B);
            Bx = zeros(length(x),length(u));
            for k = 1:p
                Bx(:,k) = fds.B(:,:,k)*x;
            end
            
            dx = (fds.A*x + fds.a) + (Bx + fds.b)*u;
        end
    end
end

