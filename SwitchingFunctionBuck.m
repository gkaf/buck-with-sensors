classdef SwitchingFunctionBuck < SwitchingFunctionAutonomousLinear
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        v_ref
        i_ref
        phi
        R
    end
    
    methods
        function [sf] = SwitchingFunctionBuck(v_ref, phi, R)
            %UNTITLED5 Construct an instance of this class
            %   Detailed explanation goes here
            i_ref = v_ref/R;
            
            zx_ref = [i_ref; v_ref];
            J_H  = [cos(phi), sin(phi)];
            
            sf@SwitchingFunctionAutonomousLinear(J_H, zx_ref)
            
            sf.v_ref = v_ref;
            sf.i_ref = v_ref/R;
            
            sf.R = R;
            
            sf.phi = phi;
        end
    end
end

