function generate_tables(database_directory, report_directory)
%generate_tables Summary of this function goes here
%   Detailed explanation goes here
import_dynamics_analysis;

[experimentAnalysis, ~] = ExperimentBatchExecution.load_results(database_directory);
experimentAnalysis.save_tables(report_directory);

path(pathdef);
end

