classdef (Abstract) SwitchingSurface
    %DynamicsFunctionSurf Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        switchingFunctionAutonomousLinear
        epsilon
    end
    
    methods
        function [df] = SwitchingSurface(switchingFunctionAutonomousLinear, epsilon)
            %DynamicsFunctionSurf Construct an instance of this class
            %   Detailed explanation goes here
            df.switchingFunctionAutonomousLinear = switchingFunctionAutonomousLinear;
            df.epsilon = sqrt(epsilon);
        end
    end
    
    methods (Abstract)
        [zz] = projected_state_space(fds, zy);
    end
    
    methods
        function [x] = hysteresis_limit(fds, zz, delta)
            zx = fds.switchingFunctionAutonomousLinear.physical_space(zz);
            
            length_zx = size(zx,2);
            z_delta = ones(1, length_zx) .* delta;
            
            x = zx + transpose(fds.switchingFunctionAutonomousLinear.J_H) * z_delta;
        end
        
        function [x_high, x_low] = hysteresis_layer(fds, zz)
            x_high = fds.hysteresis_limit(zz, fds.epsilon);
            x_low = fds.hysteresis_limit(zz, -fds.epsilon);
        end
        
        function [x_high, x_low] = custom_hysteresis_layer(fds, zy)
            zx = fds.projected_state_space(zy);
            zz = fds.switchingFunctionAutonomousLinear.normal_space(zx);
            [x_high, x_low] = fds.hysteresis_layer(zz);
        end
    end
end

