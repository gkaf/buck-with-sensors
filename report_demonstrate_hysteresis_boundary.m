function [T_traj_B, T_traj_BWS, T_HS, T_averaged_buck, T_averaged_buck_with_sensor] = report_demonstrate_hysteresis_boundary(folder)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
[dynamicsFunctionBuck, switchingFunctionBuck, epsilon_buck] = getBuck();
[dynamicsFunctionBuckWithSensors, switchingFunctionBuckWithSensors, epsilon_buck_with_sensors] = getBuckWithSensors();

circuit_buck = CircuitBuck(dynamicsFunctionBuck, switchingFunctionBuck);
circuit_buck_with_sensors = CircuitBuckWithSensors(dynamicsFunctionBuckWithSensors, switchingFunctionBuckWithSensors);

ssb = SwitchingSurfaceBuck(switchingFunctionBuck, epsilon_buck);
zy = -1:0.01:1;
[x_high, x_low] = ssb.custom_hysteresis_layer(zy);

[~, ys_buck, ~, T_buck, ys_averaged_buck] = detect_cycle(circuit_buck, epsilon_buck);
[~, ys_buck_with_sensors, ~, T_buck_with_sensors, ys_averaged_buck_with_sensor] = detect_cycle(circuit_buck_with_sensors, epsilon_buck_with_sensors);

T_traj_B = table(ys_buck(1,:)', ys_buck(2,:)', ...
    'VariableNames', {'y_1', 'y_2'});
T_traj_BWS = table(ys_buck_with_sensors(1,:)', ys_buck_with_sensors(2,:)', ys_buck_with_sensors(3,:)', ys_buck_with_sensors(4,:)', ...
    'VariableNames', {'y_1', 'y_2', 'y_3', 'y_4'});
T_HS = table(x_high(1,:)', x_high(2,:)', x_low(1,:)', x_low(2,:)', ...
    'VariableNames', {'hyst_high_1', 'hyst_high_2', 'hyst_low_1', 'hyst_low_2'});

T_averaged_buck = table(ys_averaged_buck(1), ys_averaged_buck(2), T_buck, ...
    'VariableNames', {'i_L_m', 'v_C_m', 'T_buck'});
T_averaged_buck_with_sensor = table(ys_averaged_buck_with_sensor(1), ys_averaged_buck_with_sensor(2), ...
    ys_averaged_buck_with_sensor(3), ys_averaged_buck_with_sensor(4), T_buck_with_sensors, ...
    'VariableNames', {'i_L_m', 'v_C_m', 'hat_i_L_m', 'hat_v_C_m', 'T_buck_with_sensors'});

if ~isempty(folder)
    filename = [folder, '/', 'data/hysteresis_boundary.dat'];
    writetable(T_HS, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [folder, '/', 'data/buck_trajectory.dat'];
    writetable(T_traj_B, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [folder, '/', 'data/buck_with_sensors_trajectory.dat'];
    writetable(T_traj_BWS, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [folder, '/', 'data/averaged_state_buck.dat'];
    writetable(T_averaged_buck, filename, 'WriteVariableNames', true, 'Delimiter', ';');
    
    filename = [folder, '/', 'data/averaged_state_buck_with_sensors.dat'];
    writetable(T_averaged_buck_with_sensor, filename, 'WriteVariableNames', true, 'Delimiter', ';');
end

    function [ts, ys, us, T, ys_averaged] = detect_cycle(circuit, epsilon)
        import_cycle_detection;
        
        e_tol = 1e-8;
        rel_tol = e_tol;
        abs_tol = e_tol .* ones(size(circuit.nominal_state()));
        max_step = 1e-5;
        
        max_depth = 8;
        eta_tolerance = 1e-6;
        Dt_cycle = 1;
        Dt_cross = 0.2;
        
        state_gen = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
            circuit, ...
            epsilon, max_depth, eta_tolerance, ...
            Dt_cross, Dt_cycle);
        
        ha = HybridAutomaton(state_gen);
        
        Dt_state_ub = 0.4;
        Dt_ub = 2;
        % Selecting initial state on the sliding mode:
        t_0 = 0;
        x_0 = circuit.nominal_state();
        % x_0 = circuit.dynamicsFunctionsSurf.physical_space([0;0;40]);
        
        u_0 = 1;
        initial_stateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, {}, u_0);
        hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);
        
        cycleAnalysisResult = hae.evaluate();
        
        [ts, ys, us] = cycleAnalysisResult.get_tyu();
        ys_averaged_T = trapz(transpose(ts), transpose(ys));
        T = ts(end) - ts(1);
        ys_averaged = transpose(ys_averaged_T) ./ T;
    end

path(pathdef);
end

