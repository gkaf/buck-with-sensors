function analyse_dynamics(database_directory)
%analyse_dynamics Summary of this function goes here
%   Detailed explanation goes here
import_dynamics_analysis;

experiment = getExperiment();
experimentAnalysis = experiment.get_experiment_batch();
if isa(experimentAnalysis, 'ExperimentBatch')
    experimentBatch = experimentAnalysis;
    experimentBatchExecution = ExperimentBatchExecution(database_directory, experimentBatch);
    experimentBatchExecution.execute();
else
    disp('InfeasibleExperimentalAnalysis');
end

path(pathdef);
end

