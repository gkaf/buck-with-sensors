classdef ExperimentBatch < ExperimentalAnalysis
    %ExperimentBatch
    %   Detailed explanation goes here
    
    properties
        experiment
        cases_binaryHeap
        
        % Initialization point
        x_0
        u_0
    end
    
    methods
        function [eb] = ExperimentBatch(experiment, x_0, u_0)
            %ExperimentBatch Construct an instance of this class
            %   Detailed explanation goes here
            eb@ExperimentalAnalysis();
            eb.experiment = experiment;
            eb.cases_binaryHeap = BinaryHeap();
            
            eb.x_0 = x_0;
            eb.u_0 = u_0;
            
            % Initial experiment index
            n_i = 1;
            n_v = 1;
            
            experimentCase = ExperimentCase(n_i, n_v, x_0, u_0);
            if n_i <= eb.experiment.N_i && n_v <= eb.experiment.N_v
                eb.cases_binaryHeap = eb.cases_binaryHeap.addElement(experimentCase);
            end
        end
        
        function [flag] = has_next_case(eb)
            flag = ~ eb.cases_binaryHeap.isEmpty();
        end
        
        function [eb, caseData, cycleAnalysisResult] = analyse_next_case_dynamics(eb)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            [eb.cases_binaryHeap, experimentCase] = eb.cases_binaryHeap.extractElement();
            [n_i, n_v] = experimentCase.indicies();
            [k_i, k_v] = eb.experiment.parameter_value(n_i, n_v);
            caseData = CaseData(n_i, n_v, k_i, k_v);
            
            [x_0_case, u_0_case] = experimentCase.initialization_parameters();
            hybridAutomatonEvaluation = eb.experiment.get_experiment(k_i, k_v, x_0_case, u_0_case);
            
            cycleAnalysisResult = hybridAutomatonEvaluation.evaluate();
            
            if isa(cycleAnalysisResult, 'Cycle')
                cycle = cycleAnalysisResult;
                eb.x_0 = cycle.x_0;
                eb.u_0 = cycle.u_0;
            end
            
            next_ExperimentCase_cell = experimentCase.neighbors(eb.x_0, eb.u_0);
            for n = 1:length(next_ExperimentCase_cell)
                experimentCase = next_ExperimentCase_cell{n};
                [n_i, n_v] = experimentCase.indicies();
                if n_i <= eb.experiment.N_i && n_v <= eb.experiment.N_v
                    eb.cases_binaryHeap = eb.cases_binaryHeap.addElement(experimentCase);
                end
            end
        end
    end
end

