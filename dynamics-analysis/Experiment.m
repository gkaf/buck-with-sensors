classdef Experiment
    %Experiment Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        L
        C
        E
        R
        
        v_ref
        phi
        epsilon
        
        base
        step
        N_i
        N_v
        
        e_tol
        max_step
        
        max_depth
        eta_tolerance
        Dt_cycle
        Dt_cross
        
        Dt_state_ub
        Dt_ub
    end
    
    methods
        function [e] = Experiment(L, C, E, R, ...
                v_ref, phi, epsilon, ...
                base, step, N_i, N_v, ...
                e_tol, max_step, ...
                max_depth, eta_tolerance, Dt_cycle, Dt_cross, ...
                Dt_state_ub, Dt_ub)
            %Experiment Construct an instance of this class
            %   Detailed explanation goes here
            e.L = L;
            e.C = C;
            e.E = E;
            e.R = R;
            
            e.v_ref = v_ref;
            e.phi = phi;
            e.epsilon = epsilon;
            
            e.base = base;
            e.step = step;
            e.N_i = N_i;
            e.N_v = N_v;
            
            e.e_tol = e_tol;
            e.max_step = max_step;
            
            e.max_depth = max_depth;
            e.eta_tolerance = eta_tolerance;
            e.Dt_cycle = Dt_cycle;
            e.Dt_cross = Dt_cross;
            
            e.Dt_state_ub = Dt_state_ub;
            e.Dt_ub = Dt_ub;
        end
        
        function [experimentAnalysis] = get_experiment_batch(e)
            hybridAutomatonEvaluation = e.get_ideal_experiment();
            cycleAnalysisResult = hybridAutomatonEvaluation.evaluate();
            if isa(cycleAnalysisResult, 'Cycle')
                cycle = cycleAnalysisResult;
                zx_0 = cycle.x_0;
                x_0 = [zx_0; zx_0];
                u_0 = cycle.u_0;
                
                experimentAnalysis = ExperimentBatch(e, x_0, u_0);
            else
                experimentAnalysis = InfeasibleExperimentalAnalysis();
            end
        end
        
        function [experimentAnalysis] = analyse_dynamics(e)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            cases_binaryHeap = BinaryHeap();
            experimentLog_cell = cell(e.N_i, e.N_v);
            
            hybridAutomatonEvaluation = e.get_ideal_experiment();
            cycleAnalysisResult = hybridAutomatonEvaluation.evaluate();
            if isa(cycleAnalysisResult, 'Cycle')
                cycle = cycleAnalysisResult;
                zx_0 = cycle.x_0;
                x_0 = [zx_0; zx_0];
                u_0 = cycle.u_0;
                
                n_i = 1;
                n_v = 1;
                experimentCase = ExperimentCase(n_i, n_v, x_0, u_0);
                if n_i <= e.N_i && n_v <= e.N_v
                    cases_binaryHeap = cases_binaryHeap.addElement(experimentCase);
                end
                
                while ~cases_binaryHeap.isEmpty()
                    [cases_binaryHeap, experimentCase] = cases_binaryHeap.extractElement();
                    [n_i, n_v] = experimentCase.indicies();
                    [k_i, k_v] = e.parameter_value(n_i, n_v);
                    caseData = CaseData(n_i, n_v, k_i, k_v);
                    
                    [x_0, u_0] = experimentCase.initialization_parameters();
                    hybridAutomatonEvaluation = e.get_experiment(k_i, k_v, x_0, u_0);
                    
                    cycleAnalysisResult = hybridAutomatonEvaluation.evaluate();
                    
                    experimentLog_cell{n_i, n_v} = ExperimentLog(caseData, cycleAnalysisResult);
                    
                    if isa(cycleAnalysisResult, 'Cycle')
                        cycle = cycleAnalysisResult;
                        x_0 = cycle.x_0;
                        u_0 = cycle.u_0;
                    end
                    
                    next_ExperimentCase_cell = experimentCase.neighbors(x_0, u_0);
                    for n = 1:length(next_ExperimentCase_cell)
                        experimentCase = next_ExperimentCase_cell{n};
                        [n_i, n_v] = experimentCase.indicies();
                        if n_i <= e.N_i && n_v <= e.N_v
                            cases_binaryHeap = cases_binaryHeap.addElement(experimentCase);
                        end
                    end
                end
                experimentAnalysis = FeasibleExperimentalAnalysis(e, experimentLog_cell);
            else
                experimentAnalysis = InfeasibleExperimentalAnalysis();
            end
        end
        
        function [k_i, k_v] = parameter_value(e, n_i, n_v)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            N = [e.N_i; e.N_v];
            n = [n_i; n_v];
            x = e.base + e.step*(N - n);
            k_i = x(1);
            k_v = x(2);
        end
        
        function [hybridAutomatonEvaluation] = get_experiment(e, k_i, k_v, x_0, u_0)
            dynamicsFunctionBuckWithSensors = DynamicsFunctionBuckWithSensors(e.L, e.C, e.E, e.R, k_i, k_v);
            switchingFunctionBuckWithSensors = SwitchingFunctionBuckWithSensors(e.v_ref, e.phi, e.R);
            circuit = CircuitBuckWithSensors(dynamicsFunctionBuckWithSensors, switchingFunctionBuckWithSensors);
            
            rel_tol = e.e_tol;
            abs_tol = e.e_tol .* ones(size(x_0));
            
            state_gen = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, e.max_step, ...
                circuit, ...
                e.epsilon, e.max_depth, e.eta_tolerance, ...
                e.Dt_cross, e.Dt_cycle);
            
            ha = HybridAutomaton(state_gen);
            
            % Seting initial state
            t_0 = 0;
            discreteState = {};
            
            initialStateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, discreteState, u_0);
            hybridAutomatonEvaluation = HybridAutomatonEvaluation(ha, e.Dt_state_ub, e.Dt_ub, initialStateId, t_0, x_0);
        end
        
        function [hybridAutomatonEvaluation] = get_ideal_experiment(e)
            dynamicsFunctionBuck = DynamicsFunctionBuck(e.L, e.C, e.E, e.R);
            switchingFunctionBuck = SwitchingFunctionBuck(e.v_ref, e.phi, e.R);
            circuit = CircuitBuck(dynamicsFunctionBuck, switchingFunctionBuck);
            
            x_0 = circuit.nominal_state();
            u_0 = 1;
            
            rel_tol = e.e_tol;
            abs_tol = e.e_tol .* ones(size(x_0));
            
            state_gen = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, e.max_step, ...
                circuit, ...
                e.epsilon, e.max_depth, e.eta_tolerance, ...
                e.Dt_cross, e.Dt_cycle);
            
            ha = HybridAutomaton(state_gen);
            
            % Seting initial state
            t_0 = 0;
            discreteState = {};
            
            initialStateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, discreteState, u_0);
            hybridAutomatonEvaluation = HybridAutomatonEvaluation(ha, e.Dt_state_ub, e.Dt_ub, initialStateId, t_0, x_0);
        end
    end
end

