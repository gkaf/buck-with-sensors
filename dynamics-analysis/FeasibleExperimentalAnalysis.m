classdef FeasibleExperimentalAnalysis <  ExperimentalAnalysis
    %FeasibleExperimentalAnalysis Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        experiment
        experimentLog_cell
    end
    
    methods
        function [ea] = FeasibleExperimentalAnalysis(experiment, experimentLog_cell)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            ea@ExperimentalAnalysis();
            ea.experiment = experiment;
            ea.experimentLog_cell = experimentLog_cell;
        end
        
        function [K_i, K_v] = get_paramters(ea)
            [N_i, N_v] = size(ea.experimentLog_cell);
            K_i = zeros(N_i, N_v);
            K_v = zeros(N_i, N_v);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    experimentLog = ea.experimentLog_cell{n_i, n_v};
                    [k_i, k_v] = experimentLog.get_parameters();
                    K_i(n_i, n_v) = k_i;
                    K_v(n_i, n_v) = k_v;
                end
            end
        end
        
        function [F, Delta_x, X_sys_hm0] = get_analysis_results(ea)
            [N_i, N_v] = size(ea.experimentLog_cell);
            F = zeros(N_i, N_v);
            Delta_x = cell(N_i, N_v);
            X_sys_hm0 = cell(N_i, N_v);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    experimentLog = ea.experimentLog_cell{n_i, n_v};
                    [T, delta_x, x_sys_hm0] = experimentLog.analysis();
                    F(n_i, n_v) = 1/T;
                    Delta_x{n_i, n_v} = delta_x;
                    X_sys_hm0{n_i, n_v} = x_sys_hm0;
                end
            end
        end
        
        function save_tables(ea, directory)
            if ~exist(directory, 'dir')
                mkdir(directory);
            end
            [K_i, K_v] = ea.get_paramters();
            [F, Delta_x, X_sys_hm0] = ea.get_analysis_results();
            
            filename = [directory, '/', 'frequency.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;f\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), F(n_i, n_v));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'ripple_i_L.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;Delta_i_L\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    delta_x = Delta_x{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), delta_x(1));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'ripple_v_C.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;Delta_v_C\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    delta_x = Delta_x{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), delta_x(2));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'ripple_hat_i_L.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;Delta_hat_i_L\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    delta_x = Delta_x{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), delta_x(3));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'ripple_hat_v_C.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;Delta_hat_v_C\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    delta_x = Delta_x{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), delta_x(4));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'drift_i_L.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;drift_i_L\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    x_sys_hm0 = X_sys_hm0{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), x_sys_hm0(1));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'drift_v_C.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;drift_v_C\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    x_sys_hm0 = X_sys_hm0{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), x_sys_hm0(2));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'drift_hat_i_L.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;drift_hat_i_L\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    x_sys_hm0 = X_sys_hm0{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), x_sys_hm0(3));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
            
            filename = [directory, '/', 'drift_hat_v_C.dat'];
            fileID = fopen(filename, 'w');
            fprintf(fileID, "k_i;k_v;drift_hat_v_C\n");
            [N_i, N_v] = size(ea.experimentLog_cell);
            for n_i = 1:N_i
                for n_v = 1:N_v
                    x_sys_hm0 = X_sys_hm0{n_i, n_v};
                    fprintf(fileID, "%f;%f;%f\n", K_i(n_i, n_v), K_v(n_i, n_v), x_sys_hm0(4));
                end
                fprintf(fileID, "\n");
            end
            fclose(fileID);
        end
    end
end

