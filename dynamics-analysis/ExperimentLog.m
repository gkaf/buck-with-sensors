classdef ExperimentLog
    %UNTITLED6 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        caseData
        cycleAnalysisResult
    end
    
    methods
        function [exlog] = ExperimentLog(caseData, cycleAnalysisResult)
            %UNTITLED6 Construct an instance of this class
            %   Detailed explanation goes here
            exlog.caseData = caseData;
            exlog.cycleAnalysisResult = cycleAnalysisResult;
        end
        
        function [flag] = is_a_cycle(el)
            flag = isa(el.cycleAnalysisResult, 'Cycle');
        end
        
        function [ts, ys] = get_ty(el)
            [ts, ys] = el.cycleAnalysisResult.get_ty();
        end
        
        function [k_i, k_v] = get_parameters(el)
            k_i = el.caseData.k_i;
            k_v = el.caseData.k_v;
        end
        
        function [T, delta_x, x_sys_hm0] = analysis(el)
            [ts, ys] = el.cycleAnalysisResult.get_ty();
            T = ts(end) - ts(1);
            
            X_sys = transpose(ys(1:4,:));
            min_X_sys = min(X_sys);
            max_X_sys = max(X_sys);
            delta_x = transpose(max_X_sys - min_X_sys);
            
            x_sys_int_T = trapz(transpose(ts), X_sys);
            x_sys_int = transpose(x_sys_int_T);
            x_sys_hm0 = x_sys_int ./ T;
        end
    end
end

