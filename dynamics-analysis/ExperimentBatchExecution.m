classdef ExperimentBatchExecution
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        batch_folder
        result_folder
        
        experimentBatch
    end
    
    methods
        function [ebe] = ExperimentBatchExecution(batch_folder, experimentBatch)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            ebe.batch_folder = batch_folder;
            ebe.result_folder = [batch_folder, '/', 'ExperimentLogs'];
            
            if ~exist(ebe.batch_folder, 'dir')
                mkdir(ebe.batch_folder);
                if ~exist(ebe.result_folder, 'dir')
                    mkdir(ebe.result_folder);
                end
            end
            
            ebe.experimentBatch = experimentBatch;
            experiment_batch = experimentBatch;
            
            filename = [ebe.batch_folder, '/', 'ExperimentBatch.mat'];
            save(filename, var2str(experiment_batch));
        end
        
        function execute(ebe)
            while ebe.has_next_case()
                ebe = ebe.analyse_next_case_dynamics();
            end
        end
        
        function [flag] = has_next_case(ebe)
            flag = ebe.experimentBatch.has_next_case();
        end
        
        function [ebe] = analyse_next_case_dynamics(ebe)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            eb = ebe.experimentBatch;
            [eb, caseData, cycleAnalysisResult] = eb.analyse_next_case_dynamics();
            ebe.experimentBatch = eb;
            
            experiment_log = ExperimentLog(caseData, cycleAnalysisResult);
            
            n_i = caseData.n_i;
            n_v = caseData.n_v;
            filename = [ebe.result_folder, '/', 'ExperimentLog', '_', mat2str(n_i), '_', mat2str(n_v), '.mat'];
            save(filename, var2str(experiment_log));
            
            experiment_batch = ebe.experimentBatch;
            filename = [ebe.batch_folder, '/', 'ExperimentBatch.mat'];
            save(filename, var2str(experiment_batch));
        end
    end
    
    methods (Static)
        function [experimentAnalysis, experimentBatchExecution] = load_results(batch_folder)
            filename = [batch_folder, '/', 'ExperimentBatch.mat'];
            experiment_data = load(filename);
            experiment_batch = experiment_data.experiment_batch;
            
            experimentBatchExecution = ExperimentBatchExecution(batch_folder, experiment_batch);
            ebe = experimentBatchExecution;
            
            experiment = experiment_batch.experiment;
            experimentLog_cell = cell(experiment.N_i, experiment.N_v);
            
            files = dir([ebe.result_folder, '/', '*.mat']);
            N = length(files);
            for n = 1:N
                filename = files(n).name;
                pre = split(filename, '.');
                a = split(pre{1}, '_');
                str_n_i = a(2);
                str_n_v = a(3);
                n_i = str2double(str_n_i);
                n_v = str2double(str_n_v);
                
                experimentLog = load([ebe.result_folder, '/', filename]);
                experiment_log = experimentLog.experiment_log;
                experimentLog_cell{n_i, n_v} = experiment_log;
            end
            
            no_empty_cell = true;
            n_i = 1;
            n_v = 1;
            while n_i  <= experiment.N_i && no_empty_cell
                while n_v <= experiment.N_v && no_empty_cell
                    no_empty_cell = ~ isempty(experimentLog_cell{n_i, n_v});
                    n_v = n_v + 1;
                end
                n_i = n_i + 1;
            end
            
            if no_empty_cell
                experimentAnalysis = FeasibleExperimentalAnalysis(experiment, experimentLog_cell);
            else
                experimentAnalysis = IncompleteExperimentalAnalysis();
            end
        end
    end
end

