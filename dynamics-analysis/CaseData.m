classdef CaseData
    %UNTITLED7 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        n_i
        n_v
        k_i
        k_v
    end
    
    methods
        function [cd] = CaseData(n_i, n_v, k_i, k_v)
            %UNTITLED7 Construct an instance of this class
            %   Detailed explanation goes here
            cd.n_i = n_i;
            cd.n_v = n_v;
            cd.k_i = k_i;
            cd.k_v = k_v;
        end
    end
end

