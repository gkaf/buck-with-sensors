classdef ExperimentCase < KeyedElement
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        n
        x_0
        u_0
    end
    
    methods
        function [ec] = ExperimentCase(n_i, n_v, ...
                x_0, u_0)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            ec@KeyedElement();
            ec.n = [n_i; n_v];
            
            ec.x_0 = x_0;
            ec.u_0 = u_0;
        end
        
        function [v] = key(ec)
            v = ec.n(1) + ec.n(2);
        end
        
        function [s] = toString(ec)
            s = mat2str(ec.n);
        end
        
        function [ec_cell] = neighbors(ec, x_0, u_0)
            if ec.n(2) == 1
                ec_cell = {ExperimentCase(ec.n(1)+1, ec.n(2), x_0, u_0), ...
                    ExperimentCase(ec.n(1), ec.n(2)+1, x_0, u_0)};
            else
                ec_cell = {ExperimentCase(ec.n(1), ec.n(2)+1, x_0, u_0)};
            end
        end
        
        function [n_i, n_v] = indicies(ec)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            n_i = ec.n(1);
            n_v = ec.n(2);
        end
        
        function [x_0, u_0] = initialization_parameters(ec)
            x_0 = ec.x_0;
            u_0 = ec.u_0;
        end
    end
end

