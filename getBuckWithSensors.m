function [dynamicsFunctionBuckWithSensors,switchingFunctionBuckWithSensors, epsilon] = getBuckWithSensors()
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here
L = 1.7e-3;
C = 0.6e-3;
E = 48;
phi = deg2rad(45);
v_ref = 36;
R = 8;

k_i = 5e4;
k_v = 5e4;

dynamicsFunctionBuckWithSensors = DynamicsFunctionBuckWithSensors(L, C, E, R, k_i, k_v);
switchingFunctionBuckWithSensors = SwitchingFunctionBuckWithSensors(v_ref, phi, R);

epsilon = 0.1;
end

