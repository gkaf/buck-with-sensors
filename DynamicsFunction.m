classdef (Abstract) DynamicsFunction
    %DynamicsFunction Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/DynamicsFunction.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        % Available control vectors
        U
    end
    
    methods (Abstract)
        [dx] = f_der(fsd, t, x, u);
    end
    
    methods
        function [fds] = DynamicsFunction(U)
            %DynamicsFunction Construct an instance of this class
            %   Detailed explanation goes here
            fds.U = U;
        end
    end
end

