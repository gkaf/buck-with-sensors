function [experiment] = getExperiment()
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
L = 1.7e-3;
C = 0.6e-3;
E = 48;
phi = deg2rad(45);
v_ref = 36;
R = 8;

epsilon = 0.1;

e_tol = 1e-8;
max_step = 1e-5;

max_depth = 8;
eta_tolerance = 1e-5;
Dt_cycle = 1;
Dt_cross = 0.2;

Dt_state_ub = 0.4;
Dt_ub = 4;

base = 1e3;
step = 1e3;
N_i = 200;
N_v = 200;

experiment = Experiment(L, C, E, R, ...
    v_ref, phi, epsilon, ...
    base, step, N_i, N_v, ...
    e_tol, max_step, ...
    max_depth, eta_tolerance, Dt_cycle, Dt_cross, ...
    Dt_state_ub, Dt_ub);
end

