classdef (Abstract) SwitchingRule
    %SwitchingRule Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        idx_S
        t_fin
        zx_fin
        u_post
    end
    
    methods
        function [sr] = SwitchingRule(idx_S, t_fin, zx_fin, u_post)
            %SwitchingRule Construct an instance of this class
            %   Detailed explanation goes here
            sr.idx_S = idx_S;
            sr.t_fin = t_fin;
            sr.zx_fin = zx_fin;
            sr.u_post = u_post;
        end
        
        function [v_crit] = critical_value(sr)
            %critical_value Summary of this method goes here
            %   The u_current is the intermediate value of u during the
            %   evaluation of the switch. For instance in the corener of
            %   hysteresis box, the u undergoes 2 transitions during
            %   switching.
            v_crit = sr.critical_function(sr.idx_S, sr.t_fin, sr.zx_fin, sr.u_post);
        end
        
        function [u] = get_u_post(sr)
            u = sr.u_post;
        end
    end
    
    methods (Abstract)
        [v_crit] = critical_function(sr, idx_S, t_fin, zx_fin, u_post);
    end
end

