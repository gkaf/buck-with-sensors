classdef SwitchingSurfaceBuck < SwitchingSurface
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        M_inv
    end
    
    methods
        function [ssb] = SwitchingSurfaceBuck(switchingFunctionBuck, epsilon)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            ssb@SwitchingSurface(switchingFunctionBuck, epsilon);
            J_H = switchingFunctionBuck.J_H;
            
            cos_phi = J_H(1);
            sin_phi = J_H(2);
            
            ssb.M_inv = [-sin_phi; cos_phi];
        end
        
        function [zx] = projected_state_space(ssb, zy)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            zx_ref = ssb.switchingFunctionAutonomousLinear.zx_ref * ones(size(zy));
            zx = ssb.M_inv * zy + zx_ref;
        end
    end
end

