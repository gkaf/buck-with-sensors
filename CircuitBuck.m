classdef CircuitBuck < Circuit
    %Circuit Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [circ] = CircuitBuck(dynamicsFunctionBuck, switchingFunctionBuck)
            %Circuit Construct an instance of this class
            %   Detailed explanation goes here
            n_surfaces = 1;
            n_main = 2;
            n_alternative = 2;
            
            circ@Circuit(dynamicsFunctionBuck, switchingFunctionBuck, ...
                n_surfaces, ...
                n_main, n_alternative);
        end
        
        function [f, M] = flow(circ, w)
            %flow Summary of this method goes here
            %   Detailed explanation goes here
            M = eye(2,2);
            fds = circ.dynamicsFunction;
            u = circ.switchingFunction.vector_field(w);
            
            f = @der;
            
            function [D_t_zx] = der(t, zx) % t, zx
                D_t_zx = fds.f_der(t, zx, u);
            end
        end
        
        function [switchingRule_cell] = critical_function(circ, idx_S, t_fin, zx_fin, w_pre)
            fds = circ.switchingFunction;
            
            s = fds.switching_function(t_fin, zx_fin);
            w = s <= 0;
            
            switchingRule_Stack = Stack();
            
            %DtV = fds.potential_derivative(t_fin, zx_fin, h, u_post);
            DtV = zeros(size(w));
            for n_idx = 1:length(DtV)
                DtV(n_idx) = (2*w(n_idx) - 1)*s(n_idx);
            end
            if DtV(idx_S) < 0
                w_post = w_pre;
                w_post(idx_S) = w(idx_S);
                sr = MaxConverganceRate(idx_S, t_fin, zx_fin, w_post, DtV);
                switchingRule_Stack = switchingRule_Stack.push(sr);
            end
            
            switchingRule_cell = switchingRule_Stack.toCell();
        end
        
        function [zz] = alternative_space(circ, zx)
            zz = circ.switchingFunction.normal_space(zx);
        end
    end
end

