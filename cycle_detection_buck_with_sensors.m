% z-source/simulation.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

import_cycle_detection;

[dynamicsFunctionBuckWithSensors, switchingFunctionBuckWithSensors, epsilon] = getBuckWithSensors();
circuit = CircuitBuckWithSensors(dynamicsFunctionBuckWithSensors, switchingFunctionBuckWithSensors);

e_tol = 1e-8;
rel_tol = e_tol;
abs_tol = e_tol .* ones(size(circuit.nominal_state()));
max_step = 1e-5;

max_depth = 8;
eta_tolerance = 1e-5;
Dt_cycle = 1;
Dt_cross = 0.2;

state_gen = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
    circuit, ...
    epsilon, max_depth, eta_tolerance, ...
    Dt_cross, Dt_cycle);

ha = HybridAutomaton(state_gen);

Dt_state_ub = 0.4;
Dt_ub = 2;
% Selecting initial state on the sliding mode:
t_0 = 0;
x_0 = circuit.nominal_state();
% x_0 = circuit.dynamicsFunctionsSurf.physical_space([0;0;40]);

u_0 = 1;
initial_stateId = StateId(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, {}, u_0);
hae = HybridAutomatonEvaluation(ha, Dt_state_ub, Dt_ub, initial_stateId, t_0, x_0);

cycleAnalysisResult = hae.evaluate();

[ts, ys, us] = cycleAnalysisResult.get_tyu();

path(pathdef);