classdef SwitchingSurfaceBuckWithSensors < SwitchingSurface
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        M_inv
    end
    
    methods
        function [ssb] = SwitchingSurfaceBuckWithSensors(switchingFunctionBuckWithSensors, epsilon)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            ssb@SwitchingSurface(switchingFunctionBuckWithSensors, epsilon);
            J_H = switchingFunctionBuck.J_H;
            
            cos_phi = J_H(3);
            sin_phi = J_H(4);
            
            ssb.M_inv(1,:) = [-sin_phi; cos_phi; 0; 0];
            ssb.M_inv(2,:) = [cos_phi; sin_phi; 0; 0];
            ssb.M_inv(3,:) = [0; 0; -sin_phi; cos_phi];
        end
        
        function [zx] = projected_state_space(ssb, zy)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            zx_ref = ssb.switchingFunctionAutonomousLinear.zx_ref * ones(size(zy));
            zx = ssb.M_inv * zy + zx_ref;
        end
    end
end

