classdef DynamicsFunctionBuckWithSensors < BilinearSystem
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        L
        C
        
        E
        R
        
        k_i
        k_v
    end
    
    methods
        function [fds] = DynamicsFunctionBuckWithSensors(L, C, ...
                E, R, ...
                k_i, k_v)
            %UNTITLED8 Construct an instance of this class
            %   Detailed explanation goes here
            U = [0,1];
            
            A = [0, -1/L, 0, 0;
                1/C, -1/(R*C), 0, 0;
                k_i, 0, -k_i, 0;
                0, k_v, 0, -k_v];
            B = zeros(4,4,1);
            a = zeros(4,1);
            b = [E/L; 0; 0; 0];
            
            fds@BilinearSystem(U, A, B, a, b);
            
            fds.L = L;
            fds.C = C;
            fds.E = E;
            fds.R = R;
            
            fds.k_i = k_i;
            fds.k_v = k_v;
        end
    end
end

