classdef CycleDetectionDiscreteStateEnumeration < DiscreteStateEnumeration
    %FilterDiscreteStateEnumeration Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/FilterDiscreteStateEnumeration.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    enumeration
        % Convergence detection
        StartCycleDetection
        ReachBoundary
        
        CheckConvergence
        
        StartEvaluation
        
        % Simple simulation
        FiniteStepEvaluation
        FiniteTimeEvaluation
    end
    
    methods
        function [fdse] = CycleDetectionDiscreteStateEnumeration()
            fdse@DiscreteStateEnumeration();
        end
    end
    
end

