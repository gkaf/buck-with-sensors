classdef ReachBoundary < OutputFunctionsFlow & DiscreteStateFlow
    %UNTITLED26 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        epsilon
        % Info to detect unstable sliding surface
        Dt_cross
        % Parameters to initialize convergence check
        max_search_depth
        eta_tolerance
    end
    
    methods
        function [ds] = ReachBoundary(trajectoryCheckPoints, u, ...
                circuit, ...
                epsilon, ...
                max_search_depth, eta_tolerance, ...
                Dt_cross, ...
                flowIntegrator, ...
                continuum_output_size)
            %ReachBoundary Construct an instance of this class
            %   Detailed explanation goes here
            ds@OutputFunctionsFlow(circuit);
            ds@DiscreteStateFlow(CycleDetectionDiscreteStateEnumeration.ReachBoundary, trajectoryCheckPoints, u, ...
                flowIntegrator, Interval.FinalState, ...
                continuum_output_size)
            
            ds.epsilon = epsilon;
            ds.Dt_cross = Dt_cross;
            
            ds.max_search_depth = max_search_depth;
            ds.eta_tolerance = eta_tolerance;
        end
        
        function [f, M] = flow(ds, ~ , ~) % ds, t, x
            n = ds.continuumOutputSize() - ds.compressedContinuumOutputSize();
            M = eye(n,n);
            
            u = ds.discreteOutput();
            f = ds.circuit.flow(u);
        end
        
        function [e, idx_terminal_bitmap] = event(ds, ~, x)
            % u = [u_1; u_2]
            u = ds.discreteOutput();
            
            if ismember(u(1), [0,1])
                e = getEventsFcn();
            else
                e = {};
            end
            
            epsilon_event = ds.epsilon;
            t_init = ds.starting_time(x);
            Dt_cross_max = ds.Dt_cross;
            
            idx_terminal_bitmap = [ones(size(epsilon_event)); 1];
            direction_potential = [ones(size(epsilon_event)); -1];
            
            function [e] = getEventsFcn()
                e = @eventsFcn;
                
                function [position, isterminal, direction] = eventsFcn(t, zx)
                    v = ds.circuit.potential_function(t, zx);
                    position = [v - epsilon_event; ...
                        Dt_cross_max - (t - t_init)]; % The value that we want to be zero
                    isterminal = idx_terminal_bitmap; % Halt integration
                    direction = direction_potential;
                end
            end
        end
        
        function [stateTransition] = transition(ds, flowTranscript)
            e_idx = flowTranscript.e_idx();
            if isempty(e_idx)
                discreteStateData = TimeRunOut();
                stateTransition = StateTerminal(discreteStateData);
            else
                t_fin = flowTranscript.finalTimeInstance();
                x_fin = flowTranscript.finalState();
                zx_fin = ds.map(x_fin);
                
                u_new = ds.u;
                discreteStateData = {};
                wrong_idx = false;
                k = 1;
                while k <= length(e_idx) && isempty(discreteStateData) && ~wrong_idx
                    if e_idx(k) <= ds.circuit.n_surfaces
                        idx_S = e_idx(k);
                        switchingRule_cell = ds.circuit.critical_function(idx_S, t_fin, zx_fin, u_new);
                        
                        % Use the minimum critical value to avoid
                        % non-determinism.
                        N = length(switchingRule_cell);
                        v_crit = zeros(N,1);
                        for n = 1:N
                            v_crit(n) = switchingRule_cell{n}.critical_value();
                        end
                        [~, n] = min(v_crit);
                        
                        if isempty(n)
                            discreteStateData = SlidingUnstable();
                        else
                            u_new = switchingRule_cell{n}.get_u_post();
                        end
                    elseif e_idx(k) == ds.circuit.n_surfaces + 1
                        t_init = flowTranscript.initialTimeInstance();
                        x_init = flowTranscript.initialState();
                        eras_searched = 1;
                        discreteStateData = ConvergenceToSingleModeDetected(t_init, x_init, ds.u, eras_searched);
                    else
                        wrong_idx = true;
                    end
                    k = k+1;
                end
                
                if wrong_idx
                    stateTransition = {};
                elseif isa(discreteStateData, 'CycleAnalysisResult')
                    stateTransition = StateTerminal(discreteStateData);
                elseif isa(discreteStateData, 'DetectedConverganceToSingleMode')
                    discreteStateEnumeration = CycleDetectionDiscreteStateEnumeration.EvaluateTrajectory;
                    stateTransition = StateId(discreteStateEnumeration, discreteStateData, ds.u);
                else
                    discreteStateEnumeration = CycleDetectionDiscreteStateEnumeration.CheckConvergence;
                    
                    trajectoryCheckPoints = TrajectoryCheckPoints(ds.max_search_depth, ds.eta_tolerance, ...
                        t_fin, zx_fin, u_new);
                    discreteStateData = trajectoryCheckPoints;
                    
                    stateTransition = StateId(discreteStateEnumeration, discreteStateData, u_new);
                end
            end
        end
        
    end
end

