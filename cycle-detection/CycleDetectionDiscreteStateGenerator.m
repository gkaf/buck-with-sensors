classdef CycleDetectionDiscreteStateGenerator < DiscreteStateGenerator
    %CycleDetectionDiscreteStateGenerator Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/map/CycleDetectionDiscreteStateGenerator.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        flowIntegrator
        % Memoized states
        circuit
        
        % Controller hysteresis parameters
        epsilon
        
        % Evaluation parameters
        Dt_cross
        Dt_cycle
        
        % Cycle detection parameters
        max_depth
        eta_tolerance
    end
    
    methods
        function [dsg] = CycleDetectionDiscreteStateGenerator(rel_tol, abs_tol, max_step, ...
                circuit, ...
                epsilon, max_depth, eta_tolerance, ...
                Dt_cross, Dt_cycle)
            
            continuum_output_size = length(abs_tol) + circuit.n_alternative + length(epsilon);
            dsg@DiscreteStateGenerator(continuum_output_size);
            
            dsg.flowIntegrator = DynamicFlowIntegrator(rel_tol, abs_tol, max_step);
            
            dsg.epsilon = epsilon;
            dsg.max_depth = max_depth;
            dsg.eta_tolerance = eta_tolerance;
            
            dsg.Dt_cross = Dt_cross;
            dsg.Dt_cycle = Dt_cycle;
            
            % Memoize the states of the hynbrid automaton with empty
            % control input.
            dsg.circuit = circuit;
        end
        
        function [discreteState] = synthesizeState(dsg, discreteStateEnumeration, discreteStateData, u, output_size)
            switch discreteStateEnumeration
                case CycleDetectionDiscreteStateEnumeration.StartCycleDetection
                    discreteState = StartCycleDetection(discreteStateData, u, ...
                        dsg.circuit, output_size);
                case CycleDetectionDiscreteStateEnumeration.ReachBoundary
                    discreteState = ReachBoundary(discreteStateData, u, ...
                        dsg.circuit, ...
                        dsg.epsilon, ...
                        dsg.max_depth , dsg.eta_tolerance , ...
                        dsg.Dt_cross, ...
                        dsg.flowIntegrator, ...
                        output_size);
                case CycleDetectionDiscreteStateEnumeration.CheckConvergence
                    discreteState = CheckConvergence(discreteStateData, u, ...
                        dsg.circuit, ...
                        dsg.epsilon, ...
                        dsg.Dt_cross, dsg.Dt_cycle, ...
                        dsg.flowIntegrator, ...
                        output_size);
                case CycleDetectionDiscreteStateEnumeration.StartEvaluation
                    discreteState = StartEvaluation(discreteStateData, u, ...
                        dsg.circuit, output_size, ...
                        dsg);
                case CycleDetectionDiscreteStateEnumeration.FiniteStepEvaluation
                    discreteState = FiniteStepEvaluation(discreteStateData, u, ...
                        dsg.circuit, ...
                        dsg.epsilon, ...
                        dsg.flowIntegrator, ...
                        output_size);
                case CycleDetectionDiscreteStateEnumeration.FiniteTimeEvaluation
                    discreteState = FiniteTimeEvaluation(discreteStateData, u, ...
                        dsg.circuit, ...
                        dsg.epsilon, ...
                        dsg.Dt_cross, dsg.Dt_cycle, ...
                        dsg.flowIntegrator, ...
                        output_size);
                otherwise
                    discreteState = {};
            end
        end
    end
    
end

