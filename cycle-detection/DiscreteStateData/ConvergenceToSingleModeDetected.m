classdef ConvergenceToSingleModeDetected < DiscreteStateData
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t
        x
        u
        
        eras_searched
    end
    
    methods
        function [dr] = ConvergenceToSingleModeDetected(t_0, x_0, u_0, eras_searched)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            dr.t = t_0;
            dr.x = x_0;
            dr.u = u_0;
            
            dr.eras_searched = eras_searched;
        end
        
        function [t_0, x_0, u_0] = getInitilizationParameters(dr)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            t_0 = dr.t;
            x_0 = dr.x;
            u_0 = dr.u;
        end
        
        function [n] = erasSearched(dr)
            n = dr.eras_searched;
        end
    end
end

