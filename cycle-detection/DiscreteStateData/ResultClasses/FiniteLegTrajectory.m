classdef FiniteLegTrajectory < Transcript & CycleAnalysisResult
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        eras_searched
        legs
        legs_evaluated
        
        % Evaluation initialization parameters
        t_0
        x_0
        u_0
    end
    
    methods
        function [cl] = FiniteLegTrajectory(discreteStateGenerator, ...
                t_0, x_0, u_0, n, ...
                eras_searched)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            cl@Transcript(discreteStateGenerator);
            cl@CycleAnalysisResult();
            
            cl.t_0 = t_0;
            cl.x_0 = x_0;
            cl.u_0 = u_0;
            
            cl.legs = n;
            cl.legs_evaluated = 0;
            
            cl.eras_searched = eras_searched;
        end
        
        function [cl] = next_leg(cl)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            cl.legs_evaluated = cl.legs_evaluated + 1;
        end
        
        function [flag] = evaluation_complete(cl)
            flag = cl.legs_evaluated >= cl.legs;
        end
    end
end
