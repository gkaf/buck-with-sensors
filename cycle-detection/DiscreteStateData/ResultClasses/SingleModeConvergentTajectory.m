classdef SingleModeConvergentTajectory < Transcript & CycleAnalysisResult
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        eras_searched
        
        % Evaluation initialization parameters
        t_0
        x_0
        u_0
    end
    
    methods
        function [cl] = SingleModeConvergentTajectory(discreteStateGenerator, ...
                t_0, x_0, u_0, ...
                eras_searched)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            cl@Transcript(discreteStateGenerator);
            cl@CycleAnalysisResult();
            
            cl.t_0 = t_0;
            cl.x_0 = x_0;
            cl.u_0 = u_0;
            
            cl.eras_searched = eras_searched;
        end
    end
end
