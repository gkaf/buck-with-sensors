classdef Cycle < FiniteLegTrajectory
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [cl] = Cycle(discreteStateGenerator, ...
                t_0, x_0, u_0, n, ...
                eras_searched)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            cl@FiniteLegTrajectory(discreteStateGenerator, ...
                t_0, x_0, u_0, n, ...
                eras_searched);
        end
    end
end

