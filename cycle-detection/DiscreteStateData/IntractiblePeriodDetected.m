classdef IntractiblePeriodDetected < DiscreteStateData
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t
        x
        u
        n
        eras_searched
    end
    
    methods
        function [dr] = IntractiblePeriodDetected(t_0, x_0, u_0, n, eras_searched)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            dr.t = t_0;
            dr.x = x_0;
            dr.u = u_0;
            
            dr.n = n;
            dr.eras_searched = eras_searched;
        end
        
        function [t_0, x_0, u_0, n] = getInitilizationParameters(dr)
            t_0 = dr.t;
            x_0 = dr.x;
            u_0 = dr.u;
            n = dr.n;
        end
        
        function [n] = erasSearched(dr)
            n = dr.eras_searched;
        end
    end
end

