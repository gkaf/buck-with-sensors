classdef TrajectoryCheckPoints < DiscreteStateData
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        check_point_deq
        max_depth
        
        t_0
        x_0
        u_0
        
        eta_tolerance
        eras_searched
    end
    
    methods
        function [cd] = TrajectoryCheckPoints(max_depth, eta_tolerance, ...
                t_0, x_0, u_0)
            %CycleDetection Construct an instance of this class
            %   Detailed explanation goes here
            cd@DiscreteStateData();
            
            cd.t_0 = t_0;
            cd.x_0 = x_0;
            cd.u_0 = u_0;
            
            cd.check_point_deq = DoubleEndedQueue();
            checkPoint = CheckPoint(t_0, x_0, u_0);
            cd.check_point_deq = cd.check_point_deq.addBack(checkPoint);
            
            cd.max_depth = max_depth;
            cd.eta_tolerance = eta_tolerance;
            
            cd.eras_searched = 1;
        end
        
        function [cd] = addCheckPoint(cd, t, x, u)
            %addCheckPoint Summary of this method goes here
            %   Detailed explanation goes here
            checkPoint = CheckPoint(t, x, u);
            cd.check_point_deq = cd.check_point_deq.addBack(checkPoint);
            if cd.check_point_deq.size() > cd.max_depth
                [cd.check_point_deq, ~] = cd.check_point_deq.removeFront();
                cd.t_0 = t;
                cd.x_0 = x;
                cd.u_0 = u;
            end
            cd.eras_searched = cd.eras_searched + 1;
        end
        
        function [t, x, u, n, N_eras_searched] = maximum_detection_period_exceded(cd)
            n = dc.check_point_deq.size();
            t = cd.t_0;
            x = cd.x_0;
            u = cd.u_0;
            N_eras_searched = cd.eras_searched;
        end
        
        function [detectionResult] = detect_cycle(cd, x_given)
            iter = DoubleEndedQueueIterator(cd.check_point_deq);
            
            n = 1;
            detectionResult = NotDetected();
            while iter.hasTail() && isa(detectionResult, 'NotDetected')
                checkPoint = iter.getTail();
                t = checkPoint.t;
                x = checkPoint.x;
                u = checkPoint.u;
                if norm(x_given - x, 2) < cd.eta_tolerance
                    detectionResult = Detected(t, x, u, n, cd.eras_searched);
                else
                    n = n + 1;
                    iter = iter.nextTail();
                end
            end
        end
    end
end

