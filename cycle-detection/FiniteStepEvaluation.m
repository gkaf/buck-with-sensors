classdef FiniteStepEvaluation < OutputFunctionsFlow & DiscreteStateFlow
    %StepTrajectory Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        epsilon
        % Info to detect unstable sliding surface
    end
    
    methods
        function [ds] = FiniteStepEvaluation(finiteTranscriptEvaluation, u, ...
                circuit, ...
                epsilon, ...
                flowIntegrator, ...
                continuum_output_size)
            %StepTrajectory Construct an instance of this class
            %   Detailed explanation goes here
            ds@OutputFunctionsFlow(circuit);
            ds@DiscreteStateFlow(CycleDetectionDiscreteStateEnumeration.FiniteStepEvaluation, finiteTranscriptEvaluation, u, ...
                flowIntegrator, Interval.Trajectory, ...
                continuum_output_size);
            
            ds.epsilon = epsilon;
        end
        
        function [f, M] = flow(ds, ~ , ~) % ds, t, x
            n = ds.continuumOutputSize() - ds.compressedContinuumOutputSize();
            M = eye(n,n);
            
            u = ds.discreteOutput();
            f = ds.circuit.flow(u);
        end
        
        function [e, idx_terminal_bitmap] = event(ds, ~, ~) % ds, t, x
            % u = [u_1; u_2]
            u = ds.discreteOutput();
            
            if ismember(u(1), [0,1])
                e = getEventsFcn();
            else
                e = {};
            end
            
            eps = ds.epsilon;
            
            idx_terminal_bitmap = ones(size(eps));
            direction_potential = ones(size(eps));
            
            function [e] = getEventsFcn()
                e = @eventsFcn;
                
                function [position, isterminal, direction] = eventsFcn(t, zx)
                    v = ds.circuit.potential_function(t, zx);
                    position = v - eps; % The value that we want to be zero
                    isterminal = idx_terminal_bitmap; % Halt integration
                    direction = direction_potential;
                end
            end
        end
        
        function [stateTransition] = transition(ds, flowTranscript)
            e_idx = flowTranscript.e_idx();
            if isempty(e_idx)
                discreteStateData = TimeRunOut();
                stateTransition = StateTerminal(discreteStateData);
            else
                finiteLegTrajectory = ds.discreteStateData;
                eraTranscript = FlowEraTranscript(ds, flowTranscript);
                finiteLegTrajectory = finiteLegTrajectory.extend(eraTranscript);
                finiteLegTrajectory = finiteLegTrajectory.next_leg();
                
                if finiteLegTrajectory.evaluation_complete()
                    discreteStateData = finiteLegTrajectory;
                    stateTransition = StateTerminal(discreteStateData);
                else
                    t_fin = flowTranscript.finalTimeInstance();
                    x_fin = flowTranscript.finalState();
                    zx_fin = ds.map(x_fin);
                    
                    u_new = ds.u;
                    discreteStateData = {};
                    wrong_idx = false;
                    k = 1;
                    while k <= length(e_idx) && isempty(discreteStateData) && ~wrong_idx
                        if e_idx(k) <= ds.circuit.n_surfaces
                            % Select only from relevant vctor fields (can switch, point inward)
                            idx_S = e_idx(k);
                            switchingRule_cell = ds.circuit.critical_function(idx_S, t_fin, zx_fin, u_new);
                            
                            % Use the minimum critical value to avoid
                            % non-determinism.
                            N = length(switchingRule_cell);
                            v_crit = zeros(N,1);
                            for n = 1:N
                                v_crit(n) = switchingRule_cell{n}.critical_value();
                            end
                            [~, n] = min(v_crit);
                            
                            if isempty(n)
                                discreteStateData = SlidingUnstable();
                            else
                                u_new = switchingRule_cell{n}.get_u_post();
                            end
                        else
                            wrong_idx = true;
                        end
                        k = k+1;
                    end
                    
                    if wrong_idx
                        stateTransition = {};
                    elseif isempty(discreteStateData)
                        discreteStateData = finiteLegTrajectory;
                        discreteStateEnumeration = CycleDetectionDiscreteStateEnumeration.FiniteStepEvaluation;
                        stateTransition = StateId(discreteStateEnumeration, discreteStateData, u_new);
                    else
                        stateTransition = StateTerminal(discreteStateData);
                    end
                end
            end
        end
        
    end
end

