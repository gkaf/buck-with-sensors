classdef CheckPoint
    %CheckPoint Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t
        x
        u
    end
    
    methods
        function [cp] = CheckPoint(t, x, u)
            %CheckPoint Construct an instance of this class
            %   Detailed explanation goes here
            cp.t = t;
            cp.x = x;
            cp.u = u;
        end
    end
end

