classdef Detected < DetectionResult
    %Detected Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t_0
        x_0
        u_0
        n
        eras_searched
    end
    
    methods
        function [dr] = Detected(t_0, x_0, u_0, n, eras_searched)
            %UNTITLED21 Construct an instance of this class
            %   Detailed explanation goes here
            dr@DetectionResult();
            dr.t_0 = t_0;
            dr.x_0 = x_0;
            dr.u_0 = u_0;
            dr.n = n;
            dr.eras_searched = eras_searched;
        end
        
        function [t_0, x_0, u_0, n] = getInitilizationParameters(dr)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            t_0 = dr.t_0;
            x_0 = dr.x_0;
            u_0 = dr.u_0;
            n = dr.n;
        end
        
        function [n] = erasSearched(dr)
            n = dr.eras_searched;
        end
    end
end

