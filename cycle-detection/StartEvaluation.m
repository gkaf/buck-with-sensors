classdef StartEvaluation < OutputFunctions & DiscreteStateJump
    %StartCycleEvaluation Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/Initial.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        cycleDetectionDiscreteStateGenerator
    end
    
    methods
        function [ds] = StartEvaluation(discreteStateData, u, circuit, continuum_output_size, ...
                cycleDetectionDiscreteStateGenerator)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here
            
            ds@OutputFunctions(circuit);
            ds@DiscreteStateJump(CycleDetectionDiscreteStateEnumeration.StartEvaluation, discreteStateData, u, continuum_output_size);
            %flowIntegrator, interval, ...
            ds.cycleDetectionDiscreteStateGenerator = cycleDetectionDiscreteStateGenerator;
        end
        
        function [e_idx] = guard(ds, ~, ~) % ds, t, x
            if isa(ds.discreteStateData, 'Detected')
                e_idx = 1;
            elseif isa(ds.discreteStateData, 'ConvergenceToSingleModeDetected')
                e_idx = 2;
            elseif isa(ds.discreteStateData, 'IntractiblePeriodDetected')
                e_idx = 3;
            else
                e_idx = 0;
            end
        end
        
        function [stateTransition, t_post, x_post] = jump(ds, e_idx, t_pre, x_pre) % ds, e_idx, t_pre, x_pre
            if e_idx == 1
                detected = ds.discreteStateData;
                [t_0, x_0, u_0, n] = detected.getInitilizationParameters();
                t_post = t_0;
                x_post = [x_0; t_0];
                
                eras_searched = detected.erasSearched();
                cycle = Cycle(ds.cycleDetectionDiscreteStateGenerator, ...
                    t_0, x_0, u_0, n, ...
                    eras_searched);
                eraTranscript = JumpEraTranscript(ds, t_post, x_post, e_idx);
                cycle = cycle.extend(eraTranscript);
                discreteStateData = cycle;
                
                discreteStateEnumeration = CycleDetectionDiscreteStateEnumeration.FiniteStepEvaluation;
                
                stateTransition = StateId(discreteStateEnumeration, discreteStateData, u_0);
            elseif e_idx == 2
                converganceToSingleModeDetected = ds.discreteStateData;
                [t_0, x_0, u_0] = converganceToSingleModeDetected.getInitilizationParameters();
                t_post = t_0;
                x_post = [x_0; t_0];
                
                eras_searched = converganceToSingleModeDetected.erasSearched();
                singleModeConvergentTajectory = SingleModeConvergentTajectory(ds.cycleDetectionDiscreteStateGenerator, ...
                    t_0, x_0, u_0, ...
                    eras_searched);
                eraTranscript = JumpEraTranscript(ds, t_post, x_post, e_idx);
                singleModeConvergentTajectory = singleModeConvergentTajectory.extend(eraTranscript);
                discreteStateData = singleModeConvergentTajectory;
                
                discreteStateEnumeration = CycleDetectionDiscreteStateEnumeration.FiniteTimeEvaluation;
                
                stateTransition = StateId(discreteStateEnumeration, discreteStateData, u_0);
            elseif e_idx == 3
                intractiblePeriodDetected = ds.discreteStateData;
                [t_0, x_0, u_0, n] = intractiblePeriodDetected.getInitilizationParameters();
                t_post = t_0;
                x_post = [x_0; t_0];
                
                eras_searched = intractiblePeriodDetected.erasSearched();
                intractiblePeriodTrajectory = IntractublePeriodTrajectory(ds.cycleDetectionDiscreteStateGenerator, ...
                    t_0, x_0, u_0, n, ...
                    eras_searched);
                eraTranscript = JumpEraTranscript(ds, t_post, x_post, e_idx);
                intractiblePeriodTrajectory = intractiblePeriodTrajectory.extend(eraTranscript);
                discreteStateData = intractiblePeriodTrajectory;
                
                discreteStateEnumeration = CycleDetectionDiscreteStateEnumeration.FiniteStepEvaluation;
                
                stateTransition = StateId(discreteStateEnumeration, discreteStateData, u_0);
            else
                stateTransition = {};
                t_post = t_pre;
                x_post = x_pre;
            end
        end
    end
    
end

