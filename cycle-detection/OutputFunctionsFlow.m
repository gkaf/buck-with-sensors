classdef (Abstract) OutputFunctionsFlow < OutputFunctions
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        n_after_main
    end
    
    methods
        function [fds] = OutputFunctionsFlow(circuit)
            %UNTITLED8 Construct an instance of this class
            %   Detailed explanation goes here
            fds@OutputFunctions(circuit)
            fds.n_after_main = circuit.n_main + 1;
        end
        
        function [zx] = map(fds, x)
            zx = x(1:fds.main_end);
        end
        
        function [hp] = getHolographicProjection(fds)
            hp = CycleDetectionHolographicProjection(fds.n_after_main);
        end
        
        function [t_init] = starting_time(fds, x_init)
            t_init = x_init(fds.n_after_main);
        end
    end
end

