classdef (Abstract) OutputFunctions
    %UNTITLED6 Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/MapFunctions.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        circuit
        
        main_end
        alternative_start
    end
    
    methods
        function [fds] = OutputFunctions(circuit)
            %FilterDiscreteState Construct an instance of this class
            %   Detailed explanation goes here
            fds.circuit = circuit;
            
            fds.main_end = circuit.n_main;
            fds.alternative_start = fds.main_end + 1;
        end
        
        function [y] = continuumOutput(fds, t, x) % fds, t, x
            zx = x(1:fds.main_end);
            y = [zx; % 3
                fds.circuit.alternative_space(zx); % 3
                fds.circuit.potential_function(t, zx)]; % 1
        end
        
        function [zy] = compressContinuumOutput(fds, y)
            zy = y(fds.alternative_start : end); % 4:7
        end
        
        function [n] = compressedContinuumOutputSize(fds)
            n = fds.circuit.n_alternative + fds.circuit.n_surfaces; % 4
        end
        
        function [oh] = getOutputHandler(fds)
            oh = CycleDetectionOutputHandler(fds.main_end);
        end
    end
end

