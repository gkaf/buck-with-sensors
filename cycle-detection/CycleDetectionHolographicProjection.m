classdef CycleDetectionHolographicProjection < HolographicProjection
    %CycleDetectionHolographicProjection Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        n_after_main
    end
    
    methods
        function [hp] = CycleDetectionHolographicProjection(n_after_main)
            %CycleDetectionHolographicProjection Construct an instance of this class
            %   Detailed explanation goes here
            hp@HolographicProjection();
            hp.n_after_main = n_after_main;
        end
        
        function [x] = hologram(hp, x_init, zx)
            t_init = x_init(hp.n_after_main);
            x = [zx; t_init];
        end
    end
end

