classdef StartCycleDetection < OutputFunctions & DiscreteStateJump
    %StartCycleDetection Summary of this class goes here
    %   Detailed explanation goes here
    
    % z-source/StartDetection.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods
        function [ds] = StartCycleDetection(discreteStateData, u, ...
                circuit, ...
                continuum_output_size)
            %StartCycleDetection Construct an instance of this class
            %   Detailed explanation goes here
            ds@OutputFunctions(circuit);
            ds@DiscreteStateJump(CycleDetectionDiscreteStateEnumeration.StartCycleDetection, ...
                discreteStateData, u, continuum_output_size);
        end
        
        function [e_idx] = guard(~, ~, ~) % ds, t, x
            e_idx = 1;
        end
        
        function [stateTransition, t_post, x_post] = jump(ds, ~, t_pre, x_pre) % ds, e_idx, t, x_pre
            discreteStateEnumeration = CycleDetectionDiscreteStateEnumeration.ReachBoundary;
            u = ds.u;
            discreteStateData = {};
            
            stateTransition = StateId(discreteStateEnumeration, discreteStateData, u);
            
            t_post = t_pre;
            x_post = [x_pre; t_pre];
        end
    end
end

