function [dynamicsFunctionBuck, switchingFunctionBuck, epsilon] = getBuck()
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here
L = 1.7e-3;
C = 0.6e-3;
E = 48;
phi = deg2rad(45);
v_ref = 36;
R = 8;

dynamicsFunctionBuck = DynamicsFunctionBuck(L, C, E, R);
switchingFunctionBuck = SwitchingFunctionBuck(v_ref, phi, R);

epsilon = 0.1;
end

