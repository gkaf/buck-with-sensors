% z-source/import_flow.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

import_cycle_detection;
addpath(genpath('./dynamics-analysis'));