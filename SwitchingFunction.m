classdef (Abstract) SwitchingFunction
    %SwitchingFunction Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [sf] = SwitchingFunction()
            %SwitchingFunction Construct an instance of this class
            %   Detailed explanation goes here
        end
    end
    
    methods (Abstract)
        [u] = vector_field(sf, w);
        [v] = potential_function(sf, t, h);
        [del_zx_V, del_t_S] = potential_gradient(sf, t, zx, h);
        
        [h] = switching_function(sf, t, zx);
        
        [zz] = normal_space(sf, zx);
        [zx] = physical_space(sf, zz);
        
        [zx] = nominal_state(sf);
    end
end

