classdef DynamicsFunctionBuck < BilinearSystem
    %UNTITLED8 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        L
        C
        
        E
        R
    end
    
    methods
        function [fds] = DynamicsFunctionBuck(L, C, ...
                E, R)
            %UNTITLED8 Construct an instance of this class
            %   Detailed explanation goes here
            U = [0,1];
            
            A = [0, -1/L;
                1/C, -1/(R*C)];
            B = zeros(2,2,1);
            a = zeros(2,1);
            b = [E/L; 0];
            
            fds@BilinearSystem(U, A, B, a, b);
            
            fds.L = L;
            fds.C = C;
            fds.E = E;
            fds.R = R;
        end
    end
end

